package Database;

import Entities.Channel;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ChannelRatingDB {

    Connection con;
    PreparedStatement ps;
    PreparedStatement ps2;
    PreparedStatement ps3;
    PreparedStatement ps4;
    PreparedStatement stm;

    //Calculate the rating of a channel
    public ArrayList<Channel> ratingChannel(Channel ch) throws SQLException {
        con = ConnectionDB.conexion();

        ArrayList<Channel> ratingOfChannel = new ArrayList<>();
        int theresContent = -1;

        ps = con.prepareStatement("SELECT EXISTS(SELECT * FROM channels_contents WHERE idChannel = ?) as answer");
        ps.setInt(1, ch.getIdChannel());

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            theresContent = rs.getInt("answer");
        }
        ps.close();
        ps2 = con.prepareStatement("INSERT INTO rating_channels (ratingValue, idChannel) VALUES((SELECT AVG(ratingValue) FROM ratings_content rc"
                + " JOIN contents co ON co.idContent = rc.idContent"
                + " JOIN channels_contents chc ON chc.idContent = co.idContent"
                + " JOIN channels ch ON chc.idChannel = ch.idChannel"
                + " WHERE ch.idChannel = ?), ?)");
        ps2.setInt(1, ch.getIdChannel());
        ps2.setInt(2, ch.getIdChannel());

        ps3 = con.prepareStatement("INSERT INTO rating_channels (ratingValue, idChannel) VALUES((SELECT AVG(ratingValue) FROM rating_channels rch"
                + " JOIN channels ch ON ch.idChannel = rch.idChannel"
                + " WHERE ch.parentChannel = ? AND ch.idChannel != ?), ?)");
        ps3.setInt(1, ch.getIdChannel());
        ps3.setInt(2, ch.getIdChannel());
        ps3.setInt(3, ch.getIdChannel());

        if (theresContent == 1) {
            ps2.execute();
            ps2.close();
        } else {
            ps3.execute();
            ps3.close();
        }

        ps4 = con.prepareStatement("SELECT ch.title as title, rc.ratingValue as rating FROM rating_channels rc"
                + " JOIN channels ch ON rc.idChannel = ch.idChannel"
                + " WHERE ch.idChannel= ?");
        ps4.setInt(1, ch.getIdChannel());

        ResultSet rs4 = ps4.executeQuery();

        while (rs4.next()) {
            ratingOfChannel.add(new Channel(
                    rs4.getString("title"),
                    rs4.getInt("rating")));
        }

        ps4.close();

        con.close();

        return ratingOfChannel;
    }

    //Count number of channels
    public int numberChannels() throws SQLException {
        con = ConnectionDB.conexion();

        int numberChannels = 0;

        ps = con.prepareStatement("SELECT COUNT(idChannel) as number FROM channels");
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            numberChannels = rs.getInt("number");
        }
        ps.close();
        con.close();

        return numberChannels;

    }

    //Save ratings in csv
    public void saveRatings() throws SQLException {
        con = ConnectionDB.conexion();

        ArrayList<Channel> channelsRatings = new ArrayList<>();
        ChannelRatingDB crdb = new ChannelRatingDB();
        ChannelDB chdb = new ChannelDB();
        Channel ch_method = new Channel();
        
        //Give a rating to all the channels with rating.
        try {
            for (int i = crdb.numberChannels(); i >= 1; i--) {
                ch_method.setIdChannel(i);
                try {
                    chdb.seeChannel(ch_method);
                    crdb.ratingChannel(ch_method);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        stm = con.prepareStatement("SELECT ch.title, rch.ratingValue FROM rating_channels rch"
                + " JOIN channels ch ON ch.idChannel = rch.idChannel"
                + " ORDER BY rch.ratingValue DESC");

        ResultSet rs = stm.executeQuery();

        while (rs.next()) {
            channelsRatings.add(new Channel(
                    rs.getString("title"),
                    rs.getInt("ratingValue")));
        }
        stm.close();
        
             
        String csv = "D:\\ratings.csv";
        try {
            FileWriter fw = new FileWriter(csv, false);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);

            for (Channel channel : channelsRatings) {

                pw.println(channel.getTitle() + "," + String.valueOf(channel.getRatingValue()));
            }
            pw.flush();
            pw.close();

            JOptionPane.showMessageDialog(null, "Ratings saved");
        } catch (Exception E) {
            E.printStackTrace();
            JOptionPane.showMessageDialog(null, "Ratings not saved");
        }

    }

}
