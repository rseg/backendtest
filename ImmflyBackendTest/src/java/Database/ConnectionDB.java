package Database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
    
    public static final String URL = "jdbc:mysql://localhost:3306/backend?useSSL=false&serverTimezone=UTC";
    public static final String user = "root";
    public static final String password = "root";

    public static Connection conexion() {

        try {
  
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(URL, user, password);
            return con;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}