package Entities;

public class File {

    private int idFile;
    private String filePath;
    private float rating;
    private int idChannel;
    private String metadata;

    public int getIdFile() {
        return idFile;
    }

    public void setIdFile(int idFile) {
        this.idFile = idFile;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
    
    

    public int getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(int idChannel) {
        this.idChannel = idChannel;
    }

    public File(String file, String metadata) {
        this.filePath= file;
        this.metadata=metadata;
    }
    
    public File(){
        
    }

    public File(int idFile, String file, float rating, int idChannel) {
        this.idFile = idFile;
        this.filePath = file;
        this.rating = rating;
        this.idChannel = idChannel;
    }

}
