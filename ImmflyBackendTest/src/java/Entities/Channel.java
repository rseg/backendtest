package Entities;

public class Channel {

    private int idChannel;
    private String title;
    private String picture;
    private int parentChannel;
    private int ratingValue;

    public int getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(int idChannel) {
        this.idChannel = idChannel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getParentChannel() {
        return parentChannel;
    }

    public void setParentChannel(int parentChannel) {
        this.parentChannel = parentChannel;
    }

    public int getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(int ratingValue) {
        this.ratingValue = ratingValue;
    }
    
    

    public Channel() {

    }

    public Channel(String title) {
        this.title = title;
    }
    
    public Channel(int id){
        this.idChannel = id;
    }
    
    public Channel(int id, String title){
        this.idChannel = id;
        this.title = title;
    }
    
    public Channel (String title, int rating){
        this.title = title;
        this.ratingValue = rating;
    }
    
    public Channel(int id, String title, String pic) {
        this.idChannel = id;
        this.title = title;
        this.picture = pic;
        
    }

    public Channel(int id, String title, String pic, int parentCh) {
        this.idChannel = id;
        this.title = title;
        this.picture = pic;
        this.parentChannel = parentCh;
    }
    
    public Channel (int id, String title, int ratingValue){
        this.idChannel = id;
        this.title = title;
        this.ratingValue = ratingValue;
    }
}
